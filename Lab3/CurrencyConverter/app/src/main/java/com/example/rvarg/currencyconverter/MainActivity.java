package com.example.rvarg.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {



    //Declare some variables
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;
//Added the following
    String url = "https://api.fixer.io/latest?base=USD";
    //URL to extract real-time currency exchange rates, using JSON, for the

    String json = "";

    String line = "";

    String rate = "";

//Here 1
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//cast the variables to their ids
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);
        //Click event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {

            System.out.println("\nTesting 1.... Before Asynchexecution\n");

            BackgroundTask object = new BackgroundTask();

            object.execute();


//covert user's input to string
                usd = editText01.getText().toString();
//if-else statement to make sure user cannot leave the EditText blank
                if (usd.equals("")){
                    textView01.setText("This field cannot be blank!");
                } else {
//Convert string to double
                    Double dInputs = Double.parseDouble(usd);
//Convert function
                    Double result = dInputs * 112.57;
//Display the result
                    textView01.setText("$" + usd + " = " + "¥"+String.format("%.2f",
                            result));
//clear the edittext after clicking
                    editText01.setText("");
                }
            }
        });
    }
    private class BackgroundTask extends AsyncTask<Void,Void, String> {
        @Override
        protected  void onPreExecute(){super.onPreExecute();}
        @Override
        protected void onProgressUpdate(Void... values){super.onProgressUpdate(values);}
        @Override
        protected void onPostExecute (String result){
            super.onPostExecute(result);
            System.out.println("\nWhat is rate: " + rate + "\n");

            //Crucial : We must convert the String 'rate' to the type double 'value' HERE, within the Asynch task
            Double value = Double.parseDouble(rate);

            System.out.println("\nTesting JSON String Exchange Rate INSIDE AsynchTask: " + value);

            double convert = Double.parseDouble(json);

            //covert user's input to string
            usd = editText01.getText().toString();
            //if-else statement to make sure user cannot leave the EditText blank
            if (usd.equals("")){
                textView01.setText("This field cannot be blank!");
            } else {
                //Convert string to double
                Double dInputs = Double.parseDouble(usd);
                //Convert function
                Double output = dInputs * value;
                //Display the result
                textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f",
                        output));
                //clear the edittext after clicking
                editText01.setText("");
            }
        }
        @Override
        protected String doInBackground(Void... params) {
            try {


                URL web_url = new URL(MainActivity.this.url);

                HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();

                httpURLConnection.setRequestMethod("GET");

                System.out.println("\n TESTING BEFORE CONNECTION METHOD TO URL");

                httpURLConnection.connect();

                InputStream inputStream = httpURLConnection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("CONNECTION SUCCESSFUL\n");

                while(line != null){
                    line = bufferedReader.readLine();

                    json += line;
                }

                System.out.println("\nThe JSON: " + json);

                JSONObject obj = new JSONObject(json);

                JSONObject objRate = obj.getJSONObject("rates");

                rate = objRate.get("JPY").toString();
            } catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e){
                Log.e("MYAPP","unexpected JSON exception", e);
            }
       return null; }

    }
}

